# Frame Filter

This repository contains a streamlit app that allows users to filter through frames of a video, send the frame to a running MASK DETECTOR model, and display the returned annotated frame. The users can then decide if the annotation is satisfactory. If it is not, they can choose to download the image for further reannotation and model fine-tuning at the press of a button.

### Prerequisites

* `pip install -r requirements.txt`

### Streamlit app

1. Ensure that the detector model is running. Enter the URL of the model's api endpoint into "app.py" under the model endpoint section.

2. Run ```streamlit run app.py --server.maxUploadSize=<Maximum video size in megabytes>```

3. On the web app, adjust parameters:
Max features: The maximum number of objects of interest to detect per frame.
Extract per x frames: Extract and display 1 frame per this number of frames.
Sleep for x seconds: The number of seconds to display image for.

4. Upon image upload, video running starts immediately. Press the "Save image" button if the image annotation is unsatisfactory. Press "Quit" if you would like to stop.

5. Completed images are stored in "images" folder.

