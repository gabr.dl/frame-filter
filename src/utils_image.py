"""utilty scripts for image processing"""

import base64
from io import BytesIO

import cv2
import numpy as np


def encode_image(image):
    """Encode an image to base64 encoded bytes.
    
    Args
    ----
    image: PIL.PngImagePlugin.PngImageFile
    
    Returns
    -------
    base64 encoded string
    """
    buffered = BytesIO()
    image.save(buffered, format="png")
    base64_bytes = base64.b64encode(buffered.getvalue())
    return base64_bytes.decode("utf-8")


def decode_image(field):
    """Decode a base64 encoded image to a list of floats.

    Args
    ----
    field: base64 encoded string
    
    Returns
    -------
    numpy.array
    """
    array = np.frombuffer(base64.b64decode(field), dtype=np.uint8)
    image_array = cv2.imdecode(array, cv2.IMREAD_ANYCOLOR)  # BGR
    return image_array


def json2array_yolo(bboxes_json):
    """convert bbox from json to array, in format of xywhsc"""
    nested = []
    for i in bboxes_json:
        cid = 0 if i["class"] == "Face Mask" else 1 # set the cid for draw_on_image
        vertex = [i["x"], i["y"], i["width"], i["height"], i["score"], cid]
        nested.append(vertex)

    bbox_array = np.array(nested)
    return bbox_array


def draw_on_image(image, pred_arr):
    """
    Add predicted bounding boxes to image
    
    Args
    ----
    image (np.array): RGB image as numpy array
    pred_arr (np.array): Predictions correspond to xc, yc, w, h, score, class_id
    
    Returns
    -------
    numpy.array
    """
    image = np.asarray(image)
    H, W = image.shape[:2]
    
    for x, y, w, h, s, cid in pred_arr:
        label = "Face Mask" if cid == 0 else "No Face Mask"   # cid is an integer corresponding to class-id
        color = (0,255,0) if cid == 0 else (255,0,0)
        tl = int(round(0.0025 * max(H,W)))  # line thickness
        # c1, c2 = (int(W*(x-0.5*w)), int(H*(y-0.5*h))), (int(W*(x+0.5*w)), int(H*(y+0.5*h)))
        c1, c2 = (int(W*(x)), int(H*(y))), (int(W*(x+w)), int(H*(y+h)))
        cv2.rectangle(image, c1, c2, color, thickness=tl)
        
        tf = max(tl - 2, 1)  # font thickness
        s_size = cv2.getTextSize(str('{:.0%}'.format(s)),0, fontScale=float(tl) / 3, thickness=tf)[0]
        t_size = cv2.getTextSize("t", 0, fontScale=float(tl) / 3, thickness=tf)[0]
        c2 = c1[0] + t_size[0]+s_size[0]+15, c1[1] - t_size[1] -3
        cv2.rectangle(image, c1, c2 , color, -1)  # filled
        cv2.putText(image, '{}: {:.0%}'.format(label, s), (c1[0],c1[1] - 2), 0, float(tl) / 3, [0, 0, 0], thickness=tf, lineType=cv2.FONT_HERSHEY_SIMPLEX)

    return image