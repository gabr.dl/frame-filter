imutils==0.5.3
opencv_python==4.2.0.32
streamlit==0.71.0
numpy==1.18.5
requests==2.24.0
Pillow==8.0.1
