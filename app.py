"""
This script deploys a streamlit web app that extracts every nth frame of a video, sends it to a mask detection model endpoint for prediction and
displays annotated prediction. The user can then choose to download the image if the annotation is unsatisfactory for reannotation and model
retraining.
"""

import json
import time
import shutil
import os
import glob
import sys
import tempfile

import requests
import streamlit as st
from PIL import Image
import cv2

sys.path.append("src")
from utils_image import encode_image, draw_on_image, json2array_yolo

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

#create folders if not already there
if not os.path.exists('images'):
    os.makedirs('images')
if not os.path.exists('cache'):
    os.makedirs('cache')
if not os.path.exists("./videos/"):
    os.makedirs("./videos/")

#sample json data for api
json_data = \
{
  "requests": [
    {
      "features": [
        {
          "maxResults": 20,
          "type": "MASK_DETECTION"
        }
      ],
      "image": {
        "content": None
      }
    }
  ]
}

def hide_navbar():
    """hide navbar so its not apparent this is from streamlit"""
    hide_streamlit_style = """
    <style>
    #MainMenu {visibility: hidden;}
    footer {visibility: hidden;}
    </style>
    """
    st.markdown(hide_streamlit_style, unsafe_allow_html=True) 

def send2api(api, image, json_data, maxfeatures):
    """
    Sends JSON request & recieve a JSON response
    
    Args
    ----
    api (str): API endpoint
    image (image file): opened image file
    json_data (dict): json request template
    maxfeatures (int): max no. of objects to detect in image
    
    Returns
    -------
    json_response (dict): API response
    """
    base64_bytes = encode_image(image)

    # token = {"X-Bedrock-Api-Token": token}
    json_data["requests"][0]["image"]["content"] = base64_bytes
    json_data["requests"][0]["features"][0]["maxResults"] = maxfeatures

    # response = requests.post(api, headers=token, json=json_data)
    response = requests.post(api, json=json_data)
    json_response = response.content.decode('utf-8')
    json_response = json.loads(json_response)
    return json_response

def main(api):
    """design streamlit frontend"""
    #title
    st.title("Mask Detection")
    st.subheader("Instructions:")
    st.text("""
    1. Adjust parameters
    2. Upload video file
    """)

    st.sidebar.title("Change Parameters")
    maxfeatures = st.sidebar.slider("Max Features", min_value=1, max_value=50, value=20, step=1)
    frame_rate = int(st.sidebar.slider("Extract per x frames", min_value=10, max_value=240, value=60, step=10))
    sleep_time = int(st.sidebar.slider("Sleep for x seconds", min_value=1, max_value=8, value=2, step=1))
    # token = st.text_input("API Token")
    #image store
    image_store = []

    #video upload
    uploaded_file = st.file_uploader("Upload a video.")
    if uploaded_file is not None and api != "":
        #extracting frames
        video_name = uploaded_file.name.replace(".mp4", "")
        tfile = tempfile.NamedTemporaryFile(delete=False)
        tfile.write(uploaded_file.read())
        cap = cv2.VideoCapture(tfile.name)
        
        ret = True
        label_slot = st.empty()
        image_slot = st.empty()

        #read cache progress
        with open("cache.txt", "r") as f:
            progress = int(f.read())
            f.close()

        #set video to start from progress
        cap.set(1, progress)
        count = 0

        while ret:
            frame = None
            annotated_frame = None
            ret, frame = cap.read()

            if ((frame_rate+count)%frame_rate==0) and ret:
                #change coloring
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                frame = Image.fromarray(frame)
                #send to api
                json_response = send2api(api, frame, json_data, maxfeatures)
                bbox_array_list = []
                try:
                    detected = json_response["FACEMASK_DETECTION"]
                    # If there are multiple classes (eg. face mask & no face mask)
                    for classification in detected:
                        bboxes_json = classification["boundingPoly"]["normalizedVertices"]

                        # for each bbox in bboxes
                        for bbox_json in bboxes_json:
                            bbox_json["class"]=classification["boundingPoly"]["name"] # set the class to each bbox json
                        bboxes_array = json2array_yolo(bboxes_json) # then send to json2array_yolo function
                        bbox_array_list.append((bboxes_array))
                    
                    # Draw on image first
                    annotated_frame = draw_on_image(frame, bbox_array_list[0])
                    annotated_frame = Image.fromarray(annotated_frame)

                    # Draw on top of annotated frame if there are additional 
                    for i in range(1, len(bbox_array_list)):
                        bbox_array = bbox_array_list[i]
                        annotated_frame = draw_on_image(annotated_frame, bbox_array)
                        annotated_frame = Image.fromarray(annotated_frame)
                except:
                    pass

                # display returned image
                label_slot.subheader(f"Image frame {progress+count}")

                if annotated_frame:
                    image_slot.image(annotated_frame, width=800)
                else:
                    image_slot.image(frame, width=800)

                #write progress to cache
                with open("cache.txt", "w") as f:
                    f.write(str(count+progress+frame_rate))
                    f.close()
                
                #save images to cache
                cache_path = os.path.join("cache", f"{progress+count}_cached.jpg")
                frame.save(cache_path)
    
                if st.button("Save image",  key=str(count)):
                    if not os.path.exists(f"./images/{video_name}"):
                        os.makedirs(f"./images/{video_name}")
                    index = len(os.listdir(f"./images/{video_name}"))
                    shutil.copy(os.path.join("cache", f"{progress+count-frame_rate}_cached.jpg"), os.path.join("images", video_name, f"{index}_reannot.jpg"))
                if st.button("Quit", key=("quit"+str(count))):
                    ret = False

                #empty space
                for i in range(5):
                    st.text("")

                #2 second sleep
                time.sleep(sleep_time)
            count += 1
        st.subheader("Done!")


def local(api):
    print("Please select one of the following files to process:")
    for root, directory, files in os.walk("./videos"):
        for f in files:
            print(f)

    print()

    video_name = input("Enter the video name: ")
    video_name = video_name.replace(".mp4","")
    video_path = f"./videos/{video_name}.mp4"
    if not os.path.exists(video_path):
        print("Video does not exist. Please check the name.")
        sys.exit()
    
    try:
        print()
        frame_rate = int(input("Please enter the frame rate interval (eg. 60):"))
    except:
        print("Invalid frame rate interval selected. Will default to 60.")
        frame_rate = 60

    print()
    print("Your settings:")
    print(f"Selected video: {video_path}")
    print(f"Frame rate interval: {frame_rate}")
    print("The program will now run.\n")

    cap = cv2.VideoCapture(video_path)
    count = 0
    ret = True
    maxfeatures = 20
    
    while ret:
        frame = None
        annotated_frame = None
        ret, frame = cap.read()

        if ((frame_rate+count)%frame_rate==0) and ret:
            
            #change coloring
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = Image.fromarray(frame)
            
            #send to api
            json_response = send2api(api, frame, json_data, maxfeatures)
            bbox_array_list = []
            

            try:
                detected = json_response["FACEMASK_DETECTION"]
                for classification in detected:
                    # print(bbox)
                    bboxes_json = classification["boundingPoly"]["normalizedVertices"]
                    for bbox_json in bboxes_json:
                        bbox_json["class"]=classification["boundingPoly"]["name"]
                        # print(bbox_json)
                    bboxes_array = json2array_yolo(bboxes_json)
                    bbox_array_list.append((bboxes_array))
                
                annotated_frame = draw_on_image(frame, bbox_array_list[0])
                annotated_frame = Image.fromarray(annotated_frame)

                for i in range(1, len(bbox_array_list)):

                    bbox_array = bbox_array_list[i]

                    annotated_frame = draw_on_image(annotated_frame, bbox_array)
                    annotated_frame = Image.fromarray(annotated_frame)
            except:
                pass


            if not os.path.exists(f"./images/{video_name}"):
                os.makedirs(f"./images/{video_name}")
            
            ### View image first
            saved_path = f"./images/{video_name}/{count}.jpg"
            frame.save(saved_path)

            ## save `bboxes_array` txt as its own jpg so annotation tools can find it
            with open(f"./images/{video_name}/{count}.txt", "w+") as f:
                for bbox_array in bbox_array_list:
                    for x, y, w, h, s, cid in bbox_array:
                        f.write(f"{int(cid)} {x+(w/2)} {y+(h/2)} {w} {h}\n")

                f.close()

            print(f"Saved: {saved_path}")
        
        count += 1


if __name__ == "__main__":

    #model endpoint
    api = "http://localhost:5000/api"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            

    status = 0
    if len(sys.argv) > 1:
        if sys.argv[1] == "1":
            status = 1

    if status != 0:
        print("Executing local function (CLI)")
        local(api)
        print("Done! Please check the results using ybat or any other YOLO BBox annotation tool.")

    else:
        #hide navbar
        hide_navbar()

        #main program
        main(api)

        #clear image cache
        files = glob.glob(os.path.join("cache", "*"))
        for f in files:
            os.remove(f)

        #clear cache.txt
        with open("cache.txt", "w") as f:
            f.write("0")
            f.close()

    